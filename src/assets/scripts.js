function closeNotification(event){
	let parent = event.closest('.notification');
	parent.remove();
}
window.onload = (e) => {
    var el = document.querySelectorAll(".dropdown_sidebar");

	function sideBarClickHandler(event){
		let path = location.pathname;
		let all = document.querySelectorAll('.dropdown_sidebar__menu');
		let allCount = all.length;
		for (var i = 0; i < allCount; i++) {
			all[i].classList.remove('active');
		}
		let target = this.querySelector('.dropdown_sidebar__menu');
		let svg = this.querySelector('.dropdown_sidebar svg');
		if(this.classList.contains('active')){
			target.classList.remove('active');
			this.classList.remove('active');
			svg.classList.remove('active');
		}else{
			target.classList.add('active');
			this.classList.add('active');
			svg.classList.add('active');
		}
	}
	let count = el.length;
	for(let i = 0; i < count; i++){
		el[i].addEventListener("click", sideBarClickHandler, false);
	}
};

$(document).ready(function(){
	$(document).on('click', '.tab_select', function(e){
		if(!$(this).hasClass('active')){
			$('.tab').removeClass('d-block');
			$('.tab_select').removeClass('active');
			$(this).addClass('active');
			let target = $(this).attr('tab-show');
			$('.tab-' + target).addClass('d-block');
			
		}
	});
});