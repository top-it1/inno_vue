export const library = {
	data(){
		return {
			gPage: this.page,
			status: '',
			requestCategories: '',
			category: '',
			userDivisions: '',
			userPositions: '',
			position: '',
			division: '',
			page: this.page,
			pageParams: location.pathname.split('/'),
		}
	},
	methods: {
		sendAjaxRequest: function(params = null){
			this.preloader = true;
			fetch('http://localhost/api/', {
			    mode: 'cors',
			    cache: 'no-cache',
			    credentials: 'include',
				method: 'POST',
				headers:{
					'Content-Type': 'application/json;charset=utf-8'
				},
			    body: JSON.stringify(params),
			}).then((response) => {
				//console.log(response.text());
			    return response.json();
			  })
			  .then((data) => {
			  	if(data.page != null){
			  		this.selectPage(data);
			  		this.menuActive(data.page);
			  		if (data.page == 'auth'){
						window.location.href = "/#auth";
		        		window.location.reload();
		        	}
			  	}
			  	if(this.page == null){
			  		location.reload();
			  	}
				this.preloader = false;
			  	if(data.function != null && data.params != null)
			  		this[data.function](data.params);
			  	else if(data.function != null && data.params == null)
			  		this[data.function]();
			  	else
			  		return data;
			  });
		},
		ajaxSimpleRequest: function(params = null){
			fetch('http://localhost/api/', {
			    mode: 'cors',
			    cache: 'no-cache',
			    credentials: 'include',
				method: 'POST',
				headers:{
					'Content-Type': 'application/json;charset=utf-8'
				},
			    body: JSON.stringify(params),
			}).then((response) => {
				//console.log(response.text());
			    return response.json();
			  })
			  .then((data) => {

		  		if (data.page == 'auth'){
			  		this.selectPage(data);
					window.location.href = "/#auth";
					fail;
	        	}

			  	if(data.function != null && data.params != null)
			  		this[data.function](data.params);
			  	else if(data.function != null && data.params == null)
			  		this[data.function]();
			  	else
			  		return data;
			  });
		},
		sendCustomRequest: function(url = 'http://localhost/api/', params, type = 'json'){
			/*var myHeaders = new Headers();
			myHeaders.append('Content-Type', 'text/html');*/
			fetch(url, {
				mode: 'cors',
				cache: 'no-cache',
				credentials: "include",
				method: 'POST',
				body: params,
			}).then((response) => {
				//console.log(response.text());
				if(type == 'text' || type == 'html'){
					return response.text();
				}else{
					return response.json();
				}
			})
			.then((data) => {
				if(type == 'text' || type == 'html'){
					this[params.function](data);
				}else{
					if(data.function != null && data.params != null)
						this[data.function](data.params);
					else if(data.function != null && data.params == null)
						this[data.function]();
					else
						return data;
				}
			});	
		},
		getDynamicContent($page, callback = ''){
			let $this = this;
			$.ajax({
				url: 'http://localhost/templates/' + $page +'.php',
				dataType: 'html',
				success: function(data){
					if(callback != ''){
						console.log(callback);
						$this[callback](data);
					}
				},
				error: function(err){
					console.log(err);
				}
			})
		},
		setFilter: function(event, method, filterBy = {}, limit = 500, page = 1){
			let by = Object.entries(filterBy).length != 0 ? filterBy : {name: event.target.value};
			let params = {
        		method: method,
        		filter: by,
        		limit: limit,
        		page: page,
			};
			fetch('http://localhost/api/', {
			    mode: 'cors',
			    cache: 'no-cache',
			    credentials: 'include',
				method: 'POST',
				headers:{
					'Content-Type': 'application/json;charset=utf-8'
				},
			    body: JSON.stringify(params),
			}).then((response) => {
				//console.log(response.text());
			    return response.json();
			  })
			  .then((data) => {
			  	if(data.function != null && data.params != null)
			  		this[data.function](data.params);
			  	else if(data.function != null && data.params == null)
			  		this[data.function]();
			  	else
			  		return data;
			  });
		},
        selectPage: function(params = null){
        	let refresh = true;
            var hash = window.location.hash.substr(1);
            if(hash != ''){
	            if(params == null){
	                this.page = hash;
	                window.history.pushState({}, document.title, '#' + hash);
	            }
	            else{
	                this.page = params.page;
	                window.history.pushState({}, document.title, '#' + this.page);
	            }
            }else{
				this.sendCustomRequest('http://localhost/api/check/', null);
            }
	        //this.sendAjaxRequest();
	  		this.menuActive(hash);
        },
		checkResponse: function(params){
			if(params.status == 'ok'){
				window.location.href = "/#profile";
				if(location.pathname != '/admin/'){
					window.location.reload();
				}
			}else if(location.hash != '#auth' && location.pathname != '/admin/' && location.pathname != '/restore/'){
				window.location.href = "/#auth";
				window.location.reload();
			}
		},
		stringToHtml: function(str){
			var parser = new DOMParser();
			var doc = parser.parseFromString(str, 'text/html');
			return doc.body;
		},
       	toAuth: function(){
       		console.log('here');
       		window.location.href = '/#auth';
       		var hash = window.location.hash.substr(1);
       		if(hash != ''){
       			location.reload();
       		}
       		//location.reload();
       	},
        reload: function(params = null){
            var hash = window.location.hash.substr(1);
        	if(params.status == 'ok'){
              //  this.page = '404';
        		window.location.href = "/#404";
        	}else{
        		//this.page = 'auth';
				window.location.href = "/#auth";
        		//location.reload();
        	}
            /*hash = this.page;
            window.history.pushState({}, document.title, '#' + hash);*/
            
        	window.location.reload();
        },
        menuActive: function(hash){
            switch(hash){
            	case 'dictionary':
            		this.isDictionary = true;
            	break;
            	case 'structure':
            		this.isStructure = true;
            	break;
            	case 'birthdays':
            		this.isBirthdays = true;
            	break;
            	case 'inbox':
            		this.isInbox = true;
            	break;
            	case 'inbox_matching':
            		this.isInboxMatch = true;
            	break;
            	case 'inbox_perform':
            		this.isInboxPerform = true;
            	break;
            	case 'my_requests':
            		this.isMyRequests = true;
            	break;
            	case 'watching':
            		this.isWatching = true;
            	break;
            	default:

            	break;
            }
        },
		renderDateTime: function(date){
			if(date != null){
				let days = this.renderDate(date);
				if(typeof(date) == 'string'){
					date = date.replace(' ', 'T');
				}
				let h = new Date(date);

				let hour = h.getHours();
				let minute = h.getMinutes();
				if(minute == '0'){
					minute = '00';
				}else if(minute.length == 1){
					minute = minute + '0';
				}

				return days + ', ' + hour + ':' + minute;
			}
		},
		renderDate: function(date){
			if(date != null){
				if(typeof(date) == 'string'){
					date = date.replace(' ', 'T');
					if(date.indexOf('.') !== -1 && date.indexOf('.') < 10){
						date = date.replace(/\./g, '/');
					}
				}
				let d = new Date(date);
				let day = d.getDate();
				let month = this.russianMonth(d.getMonth());
				let year = d.getFullYear();

				return day + ' ' + month;
			}
		},
		russianMonth(month){
			let rus = {
				0: 'января',
				1: 'февраля',
				2: 'марта',
				3: 'апреля',
				4: 'мая',
				5: 'июня',
				6: 'июля',
				7: 'августа',
				8: 'сентября',
				9: 'октября',
				10: 'ноября',
				11: 'декабря',
			}

			return rus[month];
		},
		usersBirthday: function(params){
			this.birthdays = params;
		},
		renderRequestCategories: function(params){
			this.requestCategories = params;
		},
		renderUserDivisions: function(params){
			this.userDivisions = params;
		},
		renderUserPositions: function(params){
			this.userPositions = params;
			this.position = '';
		},
		notification: function(type, message){
			let wrap;
			let exist = document.getElementsByClassName('notification_wrap');
			if(exist.length == 0){
				wrap = document.createElement('div');
				wrap.classList.add('notification_wrap');
			}else{
				wrap = exist[0];
			}
			let notify = document.createElement('div');
			notify.classList.add('notification');
			notify.innerHTML = message + '<img src="/src/assets/img/close-white.png" onclick="closeNotification(this)" />';
			if(type == 'ok' || type == 'success'){
				notify.classList.add('notification-success');
			}
			else if(type == 'notice'){
				notify.classList.add('notification-notice');
			}else{
				notify.classList.add('notification-error');
			}
			wrap.appendChild(notify);
			document.querySelector('#main').appendChild(wrap);
			setTimeout(() => wrap.remove(), 2500);
		},
		reRenderPositions: function(params){
			let division = params.division_id;
			if(division != null){
	    		let params = {
	        		method: 'getUserPositions',
	        		division_id: division,
	        	}
	        	this.ajaxSimpleRequest(params);
			}
		},
		inDev: function(){
			this.notification('error', 'В разработке :(');
		},
		setPageTitle: function(title){
			document.title = title;
		},
		pagination_render: function(params) {
			let pages = [];

			let need_allow = false;

			if (params.max > 1)
			{
				for (let i = 1; i <= params.max; i++)
				{
					let allow = false;

					if (i == 1 || i == params.max)
						allow = true;
					else if (Math.abs(params.cur - i) <= 2)
						allow = true;

					if (allow)
					{
						if (need_allow)
						{
							pages.push('...');
							need_allow = false;
						}

						pages.push(i);
					}
					else
					{
						need_allow = true;
					}

				}
			}

			return pages;
		}
	}
}